package com.concur.dao.contract;

import com.concur.dao.data.FlightInfo;

public interface AIRService {

	int SaveFlightInfo(FlightInfo flightInfo); 
	
}
