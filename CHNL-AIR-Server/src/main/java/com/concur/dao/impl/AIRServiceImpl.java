package com.concur.dao.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Service;

import com.concur.common.util.HibernateUtil;
import com.concur.dao.contract.AIRService;
import com.concur.dao.data.FlightInfo;

@Service
public class AIRServiceImpl implements AIRService  {

	private Session session=null;
	
	public AIRServiceImpl(){
		this.session=HibernateUtil.getSessionFactory().openSession();
	}
	
	@Override
	public int SaveFlightInfo(FlightInfo flightInfo) {

        session.beginTransaction();
        session.save(flightInfo);
        session.getTransaction().commit();

		return flightInfo.getId();
	}

}
