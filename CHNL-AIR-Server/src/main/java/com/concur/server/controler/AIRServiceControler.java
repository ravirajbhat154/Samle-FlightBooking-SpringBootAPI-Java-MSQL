package com.concur.server.controler;

import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concur.dao.contract.AIRService;
import com.concur.dao.impl.AIRServiceImpl;
import com.concur.server.model.FlightInfo;

@RestController
public class AIRServiceControler {

	private AIRService airService;
	private ModelMapper modelMapper = new ModelMapper();;

	AIRServiceControler(){
		this.airService=new AIRServiceImpl() ;
		this.modelMapper = new ModelMapper();
	}
	
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public String saveFlightInfo(){
		
		return "Hi";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	public int saveFlightInfo(@RequestBody FlightInfo flightData){
		
		com.concur.dao.data.FlightInfo daoFlightInfo = modelMapper
				.map(flightData,com.concur.dao.data.FlightInfo.class);
		
		return airService.SaveFlightInfo(daoFlightInfo);
	}
}
