package com.concur.server.controler;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppStart {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		 SpringApplication.run(AppStart.class, args);

	}

}
