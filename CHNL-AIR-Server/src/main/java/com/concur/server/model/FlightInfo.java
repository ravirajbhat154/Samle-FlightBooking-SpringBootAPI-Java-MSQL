package com.concur.server.model;

import java.util.Date;

public class FlightInfo {
	
	private String flightCode;
	private String flightName;
	private String sourceLocation;
	private String destinatonLocation;
	private Date scheduleDate;

	public String getFlightCode() {
		return flightCode;
	}
	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}

	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	public String getSourceLocation() {
		return sourceLocation;
	}
	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	public String getDestinatonLocation() {
		return destinatonLocation;
	}
	public void setDestinatonLocation(String destinatonLocation) {
		this.destinatonLocation = destinatonLocation;
	}
	public Date getScheduleDate() {
		return scheduleDate;
	}
	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

}
