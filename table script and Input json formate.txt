CREATE TABLE `filght_data`.`flight_Info` (
  `id` INT NOT NULL,
  `flight_code` VARCHAR(45) NULL,
  `flight_name` VARCHAR(45) NULL,
  `source_location` VARCHAR(45) NULL,
  `destinaton_location` VARCHAR(45) NULL,
  `schedule_date` DATETIME NULL,
  `created_date` DATETIME NULL,
  `modified_date` DATETIME NULL,
  PRIMARY KEY (`id`));


{
"flightCode":"asw2",
"flightName":"Air india 1",
"sourceLocation":"bangalore",
"destinatonLocation":"Mangalore",
"scheduleDate":"2015-12-20"
}
